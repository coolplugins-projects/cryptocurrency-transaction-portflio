<?php
class CCTP_posttype
{
    /**
     * Registers our plugin with WordPress.
     */
    public static function register()
    {
        $postTypeCls = new self();

        // register hooks
        add_action('init', array($postTypeCls, 'cctp_custom_post_type'));    
        add_action('init', array($postTypeCls, 'cctp_add_role_caps'), 999);   
        add_action('add_meta_boxes', array($postTypeCls, 'cctp_register_meta_box'));
        add_filter('manage_crypto_portfolio_posts_columns', array($postTypeCls, 'set_custom_edit_cmc_columns'));
        add_action('manage_crypto_portfolio_posts_custom_column', array($postTypeCls, 'custom_cmc_column'), 10, 2);
        add_action('admin_enqueue_scripts', array($postTypeCls, 'cctp_enqueuing_admin_scripts'));   

    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        // Setup your plugin object here
    }

    public function cctp_enqueuing_admin_scripts()
    {

        wp_enqueue_style('cctp_admin_style', CCTP_URL . '/assets/css/cctp-admin-style.css', null);
        wp_enqueue_script("cctp_admin", CCTP_URL . '/assets/js/cctp-admin.js', array("jquery"));
        wp_localize_script('cctp_admin', 'cctp_data', array(
            'url' => admin_url() . 'post-new.php?post_type=crypto_portfolio',
        )
        );

    }

    public function custom_cmc_column($column, $post_id)
    {
        $data = get_post_meta($post_id, "portfolio_best_worst");
        $data = isset($data[0]) ? $data[0] : "";
        $assets = get_post_meta($post_id, "portfolio_assets");

        if (empty($data) || empty($assets[0])) {
            return;
        }
        $fiat_currency = get_option("cctp_get_currency");
        $fiat_currency = (array) $fiat_currency;
        $fiat_symbol = !empty($fiat_currency["sym"]) ? $fiat_currency["sym"] : "$";
        $fiat_rate = !empty($fiat_currency["rate"]) ? $fiat_currency["rate"] : 1;
      
        $best_performer = ($data["bestPerformancePrice"] < 0) ? '<span class="cctp_minus" style="color:red;"><span class="dashicons dashicons-arrow-down"></span>' . $this->cctp_format_number($data["bestPerformancePercentage"]) . '% (' . $fiat_symbol . ($this->cctp_format_number($data["bestPerformancePrice"])) . ')</span>' : '<span class="cctp_plus" style="color:green;"><span class="dashicons dashicons-arrow-up"></span>' . $this->cctp_format_number($data["bestPerformancePercentage"]) . '% (' . $fiat_symbol . ($this->cctp_format_number($data["bestPerformancePrice"])) . ')</span>';
        $worst_performer = ($data["worstPerformancePrice"] < 0) ? '<span class="cctp_minus" style="color:red;"><span class="dashicons dashicons-arrow-down"></span>' . $this->cctp_format_number($data["worstPerformancePercentage"]) . '% (' . $fiat_symbol . ($this->cctp_format_number($data["worstPerformancePrice"])) . ')</span>' : '<span class="cctp_plus" style="color:green;"><span class="dashicons dashicons-arrow-up"></span>' . $this->cctp_format_number($data["worstPerformancePercentage"]) . '% (' . $fiat_symbol . ($this->cctp_format_number($data["worstPerformancePrice"])) . ')</span>'; 
        $all_time_profit = ($data["allTimePrice"] < 0) ? '<span class="cctp_minus" style="color:red;"><span class="dashicons dashicons-arrow-down"></span>' . esc_html($this->cctp_format_number($data["allTimePercentage"])) . '% (' . esc_html($fiat_symbol . ($this->cctp_format_number($data["allTimePrice"]))) . ')</span>' : '<span class="cctp_plus" style="color:green;"><span class="dashicons dashicons-arrow-up"></span>' . esc_html($this->cctp_format_number($data["allTimePercentage"])) . '% (' . esc_html($fiat_symbol . ($this->cctp_format_number($data["allTimePrice"]))) . ')</span>';

        switch ($column) {
              case 'all-time-profit':
                echo '<div class="cctp_statics" style="display:flex;">' . wp_kses_post($all_time_profit) . '</div>';
                 break;         
                 case 'best-performer':
                echo '<div class="cctp_statics" style="display:flex;"><img src="' . $data["bestPerformanceLogo"] . '" title="' . $data["bestPerformanceName"] . '" height="25px">' . $best_performer . '</div>';

                 break;  
                 case 'worst-performer':
                echo '<div class="cctp_statics" style="display:flex;"><img src="' . $data["worstPerformanceLogo"] . '" title="' . $data["worstPerformanceName"] . '" height="25px">' . $worst_performer . '</div>';

                 break;  
          
        }
    }

    public function set_custom_edit_cmc_columns($columns)
    {
        $author = $columns['author'];
        $date = $columns['date'];
        unset($columns['author']);
        unset($columns['date']);
        $columns['all-time-profit'] = __('All Time Profit', 'cctp');
        $columns['best-performer'] = __('Best Performer', 'cctp');
        $columns['worst-performer'] = __('Worst Performer', 'cctp');

        
        $columns['author'] = $author;
        $columns['date'] = $date;

        return $columns;
    }

    // Register Cool Timeline Post Type
    public function cctp_custom_post_type()
    {
        $labels = array(
            'name' => _x('Portfolio', 'Post Type General Name', 'cctp2'),
            'singular_name' => _x('Portfolio', 'Post Type Singular Name', 'cctp2'),
            'menu_name' => __('Portfolio', 'cctp2'),
            'name_admin_bar' => __('Portfolio', 'cctp2'),
            'parent_item_colon' => __('Parent Item:', 'cctp2'),
            'all_items' => __('Cool Portfolio', 'cctp2'),

            'update_item' => __('Update Story', 'cctp2'),

            'search_items' => __('Search Story', 'cctp2'),
            'not_found' => __('Not found', 'cctp2'),
            'not_found_in_trash' => __('Not found in Trash', 'cctp2'),
        );
        $args = array(
            'label' => __('crypto_portfolio', 'cctp2'),
            'description' => __('Crypto portfolio Description', 'cctp2'),
            'labels' => $labels,
            'supports' => array('title', 'author'),
            'taxonomies' => array(),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            //'show_in_menu'        => 'cool-plugins-timeline-addon',
            'menu_position' => 5,
            'show_in_admin_bar' => false,
            'show_in_nav_menus' => false,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'show_in_rest' => true,
            'publicly_queryable' => true,
            'capability_type' => 'page',
             'menu_icon'=>'dashicons-chart-pie',
        );
        register_post_type('crypto_portfolio', $args);
    }

    public function cctp_add_role_caps()
    {
        $role = get_role('subscriber');
        $role->add_cap('edit_pages');
        $role->add_cap('publish_pages');
        $role->add_cap('delete_pages');

    }

   

    public function cctp_register_meta_box()
    {
        remove_meta_box('submitdiv', 'crypto_portfolio', 'side');
        add_meta_box('cctp-metabox', 'Portfolia List Table', array($this, 'cctp_list_table_meta'), 'crypto_portfolio', 'normal', 'high');
    }
    public function cctp_list_table_meta()
    {

        wp_enqueue_style("cctp-custom_css", CCTP_URL . '/assets/css/cctp-custom.css', null);
        wp_enqueue_style("Bottstrap", CCTP_URL . '/assets/css/bootstrap.min.css', null);

        wp_enqueue_style("cctp-datatable_css", CCTP_URL . '/assets/css/jquery.dataTables.min.css', null);

        wp_enqueue_script("datatable", CCTP_URL . '/assets/js/jquery.dataTables.min.js', array("jquery"));
        wp_enqueue_style('dashicons');

        wp_enqueue_script("cctp-custom_js", CCTP_URL . '/assets/js/cctp-custom.js', array("jquery"));

        $fiat_currency = get_option("cctp_get_currency");

        $fiat_currency = (array) $fiat_currency;
        
        $fiat_symbol = !empty($fiat_currency["sym"]) ? $fiat_currency["sym"] : "$";
        $fiat_rate = !empty($fiat_currency["rate"]) ? $fiat_currency["rate"] : 1;
        $fiat_curr = !empty($fiat_currency["cur"]) ? $fiat_currency["cur"] : "USD";

        $id = get_the_ID();
        $coins = new CMC_Coins();
        $data = get_post_meta($id, "my_field");
        //var_dump(get_post_meta($id, "portfolio_best_worst"));
        //var_dump(cmc_usd_conversions("INR"));
        $assets = get_post_meta($id, "portfolio_assets");
        $total_quantity = 0;
        $holding = "";
        $html = '';
        $html_body = '';
        $buy_price = 0;
        $profit_loss = "";
        $profit_loss_percent = "";
        $transaction_html = "";
        $transaction_html_body = "";
        $amount = "";
        $Total_holding = 0;
        $all_time_profit = 0;
        $logo = "";
        $logo_html = "";
        $buy_quantity = 0;
        $symbol = "";
        $sell_quantity = 0;
        $avg_sell_price = 0;
        $totla_buy_price = 0;

        foreach ($assets[0] as $keys => $values) {
            $single_coin_dat = $coins->get_coins(array("coin_id" => $values));

            $name = $single_coin_dat[0]->name;
            $price1 = $single_coin_dat[0]->price;
            $price1 = ($fiat_curr == "BTC") ? ($price1 / $fiat_rate) : ($price1 * $fiat_rate);
            $price = $this->cctp_format_number_multi($price1);
            $symbol = $single_coin_dat[0]->symbol;
            $buy_holdin = 0;
            $buy_value = "";
            $buy_value1 = "";
            $all_time_profit_percent = "";
            $sell_holdin = 0;
            $quantity = '';
            $quantity1 = '';
            

            $chnages24_per = $single_coin_dat[0]->percent_change_24h;
            $changes_24h = ($chnages24_per < 0) ? '<td class="cctp_minus"><span class="dashicons dashicons-arrow-down"></span>' . esc_html($chnages24_per) . '%</td>' : '<td class="cctp_plus"><span class="dashicons dashicons-arrow-up"></span>' . $chnages24_per . '%</td>';
            $logo = $single_coin_dat[0]->logo;
            $logo_html = '<span class="cctp_logos"><img src="' . esc_url($logo) . '"> <span class="cctp_symbol">' . esc_html($symbol) . '</span></span>';
            foreach ($data[0] as $key => $value) {
                $quantity1 = $value['quantity'];
                $quantity = $quantity1;
                // var_dump($quantity);
                $buy_value1 = ($fiat_curr == "BTC") ? $value["price"] / $fiat_rate : $value["price"] * $fiat_rate;
                $buy_value = $buy_value1;
                $amount = ($buy_value * $quantity);
                $fees = !empty($value['fee']) ? $fiat_symbol . (($fiat_curr == "BTC") ? $this->cctp_format_number($value['fee'] / $fiat_rate) : $this->cctp_format_number($value['fee'] / $fiat_rate)) : "--";

                $note = !empty($value['note']) ? $value['note'] : "--";

                if ($values == $value['coin']) {
                    if ($value['transaction_type'] == "buy") {
                        $buy_price += ($buy_value * $quantity);
                        $buy_quantity += $quantity;
                        $total_quantity += $quantity;
                        $transaction_html_body .= '<tr class="cctp_coins_' . esc_attr($values) . '" style="display:none;"><td><span class="cctp_type">' . esc_html($value['transaction_type']) . '</span><span class="cctp_date">' . esc_html($value['transaction_date']) . '</span></td> <td>' . esc_html($fiat_symbol . $this->cctp_format_number($buy_value)) . '</td><td><span class="cctp_amount">' . esc_html($fiat_symbol . $this->cctp_format_number($amount)) . '</span><span class="cctp_quantity cctp_plus">' . esc_html($quantity ). ' ' . esc_html($symbol ). '</span></td><td>' .esc_html( $fees ). '</td><td>' . esc_html($note) . '</td></tr> ';

                    } else {
                        $avg_sell_price += ($buy_value * $quantity);
                        //    $buy_price-=($buy_value*$quantity);
                        $sell_quantity += $quantity;
                        $total_quantity -= $quantity;
                        $transaction_html_body .= '<tr class="cctp_coins_' . esc_attr($values) . '" style="display:none;"><td><span class="cctp_type">' . esc_html($value['transaction_type']) . '</span><span class="cctp_date">' . esc_html($value['transaction_date']) . '</span></td> <td>' . esc_html($fiat_symbol . $this->cctp_format_number($buy_value)) . '</td><td><span class="cctp_amount">' . esc_html($fiat_symbol . $this->cctp_format_number($amount)) . '</span><span class="cctp_quantity cctp_minus">' .esc_html( $quantity) . ' ' .esc_html( $symbol) . '</span></td><td>' .esc_html( $fees ). '</td><td>' . esc_html($note) . '</td></tr> ';

                    }                                      
                    $holding = ($price*$total_quantity);


                }

            }
            $totla_buy_price += $buy_price;
          
            $sell = ($sell_quantity != 0) ? (($avg_sell_price / $sell_quantity) - $price) * $sell_quantity : 0;
            $sell = $this->cctp_format_number_multi($sell);
            $buy_avg = ($buy_quantity != 0) ? ($buy_price / $buy_quantity) : 0;
            $buy_avg  = $this->cctp_format_number_multi($buy_avg );

            $buy_pp = (($price - $buy_avg) * $buy_quantity);
            $profit_loss = $buy_pp + $sell;

            // $profit_loss = $profit_loss*$fiat_rate ;
            $profit_loss_percal = ($buy_price != 0) ? ($profit_loss / $buy_price) * 100 : 0;
            $profit_loss_percent = ($profit_loss_percal < 0) ? '<span class="cctp_minus"><span class="dashicons dashicons-arrow-down"></span>' . esc_html($this->cctp_format_number($profit_loss_percal)) . '%</span>' : '<span class="cctp_plus"><span class="dashicons dashicons-arrow-up"></span>' . esc_html($this->cctp_format_number($profit_loss_percal)) . '%</span>';
            $all_time_profit += $profit_loss;

            $Total_holding += $holding;
           
            $html_body .= '<tr id="' . esc_attr($values) . '"  data-logo="' . esc_attr($logo) . '" ><td>' . (esc_html($keys + 1)) . '</td><td>' . $logo_html . ' <span class="cctp_name">' . esc_html($name) . '</span></td> <td>' . esc_html($fiat_symbol . $this->cctp_format_number($price)) . '</td>' . $changes_24h . '<td><span class="cctp_holding">' . esc_html($fiat_symbol . $this->cctp_format_number($holding)) . '</span><span class="cctp_quantity">' . esc_html($this->cctp_format_number($total_quantity)) . ' ' . esc_html($symbol) . '</span></td><td>' . esc_html($fiat_symbol . $this->cctp_format_number($buy_avg)) . '</td><td><span class="cctp_profitloss">' . esc_html($fiat_symbol . $this->cctp_format_number($profit_loss)) . '</span><span class="cctp_profitloss_percent">' . $profit_loss_percent . '</span></td><td><span class="cctp_coins button button-primary button-small"  id="' . esc_attr($values) . '"  data-logo="' . esc_attr($logo) . '" >' . __("View Transactions", "cctp") . '</span></td></tr> ';
            $total_quantity = 0;
            $buy_price = 0;
            $buy_quantity = 0;
            $sell_quantity = 0;
            // $totla_buy_price = 0;

            $avg_sell_price = 0;

        }

        $all_time_profit_percent = !empty($totla_buy_price) ? ($all_time_profit / $totla_buy_price) * 100 : 0;
        $all_time_profit_html = ($all_time_profit < 0) ? '<span class="cctp_minus"><span class="dashicons dashicons-arrow-down"></span>' . esc_html($this->cctp_format_number($all_time_profit_percent)) . '% (' . $fiat_symbol . esc_html($this->cctp_format_number($all_time_profit)) . ')</span>' : '<span class="cctp_plus"><span class="dashicons dashicons-arrow-up"></span>' . esc_html($this->cctp_format_number($all_time_profit_percent)) . '% (' . $fiat_symbol . esc_html($this->cctp_format_number($all_time_profit)) . ')</span>';

        $data_best_worst = get_post_meta($id, "portfolio_best_worst");

        $data_best_worst = isset($data_best_worst[0]) ? $data_best_worst[0] : "";

        if (!empty($data_best_worst) && !empty($assets[0])) {
            $best_performer = ($data_best_worst["bestPerformancePrice"] < 0) ? '<span class="cctp_minus"><span class="dashicons dashicons-arrow-down"></span>' .esc_html($this->cctp_format_number($data_best_worst["bestPerformancePercentage"])) . '% (' . $fiat_symbol . esc_html($this->cctp_format_number($data_best_worst["bestPerformancePrice"])) . ')</span>' : '<span class="cctp_plus"><span class="dashicons dashicons-arrow-up"></span>' .esc_html($this->cctp_format_number($data_best_worst["bestPerformancePercentage"])) . '% (' . $fiat_symbol . esc_html($this->cctp_format_number($data_best_worst["bestPerformancePrice"])) . ')</span>';
            $worst_performer = ($data_best_worst["worstPerformancePrice"] < 0) ? '<span class="cctp_minus"><span class="dashicons dashicons-arrow-down"></span>' . esc_html($this->cctp_format_number($data_best_worst["worstPerformancePercentage"])) . '% (' . $fiat_symbol . esc_html($this->cctp_format_number($data_best_worst["worstPerformancePrice"])) . ')</span>' : '<span class="cctp_plus"><span class="dashicons dashicons-arrow-up"></span>' . esc_html($this->cctp_format_number($data_best_worst["worstPerformancePercentage"])) . '% (' . $fiat_symbol . esc_html($this->cctp_format_number($data_best_worst["worstPerformancePrice"])) . ')</span>';

            $html .= '<div class="cctp_total_holding"><span class="cctp_current_balance">' . __("Current Balance", "cctp") .'</span><h1>'  . esc_html($fiat_symbol . $this->cctp_format_number($Total_holding)) . '</h1></div><div class="cctp_statics"><span class="cctp_all_time_profit">' . __("All Time Profit", "cctp") . ' <br>' . $all_time_profit_html . '</span><div class="cctp_best_perform_wrap"><img src="' . esc_url($data_best_worst["bestPerformanceLogo"]) . '" title="' . esc_attr($data_best_worst["bestPerformanceName"]) . '"><span class="cctp_best_performer ">' . __("Best Performer", "cctp") . '<br>' .$best_performer . '</span></div><div class="cctp_best_perform_wrap"><img src="' . esc_url($data_best_worst["worstPerformanceLogo"]) . '" title="' . esc_attr($data_best_worst["worstPerformanceName"]) . '"><span class="cctp_worst_performer">Worst Performer<br>' . $worst_performer . '</span></div></div>';
        }
        $html .= '<div class="table-responsive"><table id="cctp_admin_list" class="display compact " ><thead class="thead-dark"><tr><th>' . __("S.no", "cctp") . '</th><th>' . __("Name", "cctp") . '</th> <th>' . __("Price", "cctp") . '</th><th>' . __("24H", "cctp") . '</th><th>' . __("Holding", "cctp") . '</th><th>' . __("Avg. Buy Price", "cctp") . '</th><th>' . __("Profit /loss", "cctp") . '</th><th>' . __("Transactions", "cctp") . '</th></tr></thead><tbody>';
        $transaction_html .= '<div class="cctp_modal_wraper"><div class="cctp_modal" style="display: none;"><span class="cctp_close_modal"><span class="cctp_modal_close dashicons dashicons-no"></span></span><div class="cctp_coins_transactions"></div><div class="table-responsive"><table id="cctp_transaction_list"  class="cctp_transaction_list table"><thead><tr><th>' . __("Type", "cctp") . '</th> <th>' . __("Price", "cctp") . '</th><th>' . __("Amount", "cctp") . '</th><th>' . __("Fees", "cctp") . '</th><th>' . __("Note", "cctp") . '</th></tr></thead><tbody class="modal-content">';
        $transaction_html .= $transaction_html_body;
        $html .= $html_body;
        $transaction_html .= '</tbody></table></div></div></div>';
        $html .= '</tbody></table></div>';

        echo wp_kses_post($transaction_html);
        echo wp_kses_post($html);
    }
    public function cctp_format_number($n)
    {

        if ($n >= 1) {
            return $formatted = number_format($n, 2, '.', ',');
        } else if ($n >= 0.50 && $n < 25) {
            return $formatted = number_format($n, 3, '.', ',');
        } else if ($n >= 0.01 && $n < 0.5) {
            return $formatted = number_format($n, 4, '.', ',');
        } else if ($n >= 0.0001 && $n < 0.01) {
            return $formatted = number_format($n, 5, '.', ',');
        } else if ($n >= 0.00001 && $n < 0.0001) {
            return $formatted = number_format($n, 6, '.', ',');
        } else if ($n >= 0.000001 && $n < 0.00001) {
            return $formatted = number_format($n, 8, '.', ',');
        } else if ($n <= -0.01) {
            return $formatted = number_format($n, 2, '.', ',');
        } else if ($n <= -0.001) {
            return $formatted = number_format($n, 3, '.', ',');
        } else if ($n <= -0.00001) {
            return $formatted = number_format($n, 5, '.', ',');
        } else if ($n <= 0) {
            return $formatted = number_format($n, 2, '.', ',');
        } else {
            return $formatted = number_format($n, 8, '.', ',');
        }
    }
        public function cctp_format_number_multi($n)
    {

        if ($n >= 25) {
            return $formatted = number_format($n, 2, '.', '');
        } else if ($n >= 0.50 && $n < 25) {
            return $formatted = number_format($n, 3, '.', '');
        } else if ($n >= 0.01 && $n < 0.5) {
            return $formatted = number_format($n, 4, '.', '');
        } else if ($n >= 0.0001 && $n < 0.01) {
            return $formatted = number_format($n, 5, '.', '');
        } else if ($n >= 0.00001 && $n < 0.0001) {
            return $formatted = number_format($n, 6, '.', '');
        } else if ($n >= 0.000001 && $n < 0.00001) {
            return $formatted = number_format($n, 8, '.', '');
        } else if ($n <= -0.01) {
            return $formatted = number_format($n, 2, '.', '');
        } else if ($n <= -0.001) {
            return $formatted = number_format($n, 3, '.', '');
        } else if ($n <= -0.00001) {
            return $formatted = number_format($n, 5, '.', '');
        } else if ($n <= 0) {
            return $formatted = number_format($n, 2, '.', '');
        } else {
            return $formatted = number_format($n, 8, '.', '');
        }
    }


}
CCTP_posttype::register();
