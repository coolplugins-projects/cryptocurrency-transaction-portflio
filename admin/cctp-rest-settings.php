<?php
class CCTP_rest_settings
{
    /**
     * Registers our plugin with WordPress.
     */
    public static function register()
    {
        $restCls = new self();
        // register hooks
        add_action('rest_api_init', array($restCls, 'cctp_register_posts_meta_field'));      
        add_action('init', array($restCls, 'cctp_add_restrout'));   

    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        // Setup your plugin object here
    }

  
    public function cctp_register_posts_meta_field()
    {
        $field = 'my_field';
        register_rest_field(
            'crypto_portfolio',
            $field,
            array(
                'get_callback' => function ($object) use ($field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $field, true);
                },
                'update_callback' => function ($value, $object) use ($field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $field, $value);
                },
            ));
        $portfolio_field = 'total_portfolio';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_field,
            array(
                'get_callback' => function ($object) use ($portfolio_field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_field, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_field, $value);
                },
            ));
        $portfolio_best_worst = 'portfolio_best_worst';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_best_worst,
            array(
                'get_callback' => function ($object) use ($portfolio_best_worst) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_best_worst, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_best_worst) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_best_worst, $value);
                },
            ));
        $portfolio_field = 'portfolio_assets';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_field,
            array(
                'get_callback' => function ($object) use ($portfolio_field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_field, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_field, $value);
                },
            ));
        $portfolio_field = 'portfolio_fiat_currency';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_field,
            array(
                'get_callback' => function ($object) use ($portfolio_field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_field, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_field, $value);
                },
            ));
        $portfolio_field = 'portfolio_color_scheme';
        register_rest_field(
            'crypto_portfolio',
            $portfolio_field,
            array(
                'get_callback' => function ($object) use ($portfolio_field) {
                    // Get field as single value from post meta.
                    return get_post_meta($object['id'], $portfolio_field, true);
                },
                'update_callback' => function ($value, $object) use ($portfolio_field) {
                    // Update the field/meta value.
                    update_post_meta($object->ID, $portfolio_field, $value);
                },
            ));

    }

    public function cctp_add_restrout()
    {
        // rest api endpoint for sitemap generation
        add_action('rest_api_init', function () {
            register_rest_route('coin-market-cap/v1', 'cmc_get_top_100_coins', array(
                'methods' => 'GET',
                'callback' => array($this, 'cmc_get_top_100_coins'),
                'permission_callback' => '__return_true',
            ));
            register_rest_route('coin-market-cap/v1', 'cmc_get_rest_historical_data', array(
                'methods' => 'GET',
                'callback' => array($this, 'cmc_get_rest_historical_data'),
                'permission_callback' => '__return_true',
            ));
            register_rest_route('coin-market-cap/v1', 'cmc_get_fiat_price', array(
                'methods' => 'GET',
                'callback' => array($this, 'cmc_get_fiat_price'),
                'permission_callback' => '__return_true',
            ));
            register_rest_route('coin-market-cap/v1', 'cctp_save_fiat_currency', array(
                'methods' => 'POST',
                'callback' => array($this, 'cctp_save_fiat_currency'),
                'permission_callback' => '__return_true',
            ));

        });
    }

    public function cmc_get_fiat_price()
    {

        $response['status'] = "success";
        $response['data'] = cmc_usd_conversions("all");
        echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        die();

    }

    public function cmc_get_top_100_coins()
    {
        $cmcDB = new CMC_Coins;
        $coindata = $cmcDB->get_top_changers_coins(array("number" => "100"));
        $response['status'] = "success";
        $response['data'] = $coindata;
        echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        die();

    }

    public function cmc_get_rest_historical_data($data)
    {
        $meta_tbl = new CMC_Coins_historical();
        $coin_symbol = $data->get_param('coin_id');
        $no_days = $data->get_param('days');
        if (empty($coin_symbol)) {
            $resposne['status'] = 'fail';
            $resposne['data'] = 'null';
            $resposne['Note'] = 'Required coin id missing';
            echo json_encode($resposne, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
            die();
        }
        $historical_data1 = get_transient("cmc-" . $coin_symbol . '-history-data-24H');
        $historical_data365 = get_transient("cmc-" . $coin_symbol . '-history-data-365');
        $day = isset($no_days) ? $no_days : '365';
        $transient = ($day == 2) ? $historical_data1 : $historical_data365;
        $resposne['status'] = 'success';
        $resposne['data'] = (empty($transient)) ? cmc_historical_chart_json($coin_symbol, $day) : $meta_tbl->cmc_get_historical_data($coin_symbol, $day);
        echo json_encode($resposne, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        die();

    }

    public function cctp_save_fiat_currency($response)
    {
        $data = $response->get_params();

        if (isset($data["portfolio_fiat_currency"]) && !empty($data["portfolio_fiat_currency"])) {
            update_option("cctp_get_currency", json_decode($data["portfolio_fiat_currency"]));

        }
        die();
    }

   

}
CCTP_rest_settings::register();
