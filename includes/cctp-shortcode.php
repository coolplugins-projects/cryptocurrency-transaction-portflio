<?php
class CCTP_shortcodes
{
    /**
     * Registers our plugin with WordPress.
     */
    public static function register()
    {
        $postTypeCls = new self();

        // register hooks
        add_shortcode('cctp_portfolio', array($postTypeCls, 'cctp_portfolio'));  
        add_shortcode('coin-market-cap-currency', array($postTypeCls, 'cctp_currency_conversion'));
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        // Setup your plugin object here
    }

   
    public function cctp_portfolio()
    {
        // $this->wpshout_save_post_if_submitted();

        if (is_user_logged_in()) {

            wp_enqueue_style('cctp-portfolio-list-css');
            wp_enqueue_style('cctp-portfolio-modal-css');
            wp_enqueue_style('cctp-portfolio-loader');
            wp_enqueue_script('cctp-build-portfolio', plugin_dir_url(__DIR__) . 'includes/portfolio-tracker/dist/bundle.js', ['jquery', 'wp-element'], wp_rand(), true, true);
            wp_localize_script('cctp-build-portfolio', 'appLocalizer', [
                'apiUrl' => home_url(),
                'nonce' => wp_create_nonce('wp_rest'),
                'author_id' => get_current_user_id(),
            ]);
            return '<div class="cctp-wrap"><div id="cctp-portfolio-app"></div></div>';

        }
    }

   
    public function cctp_currency_conversion($atts, $content = null)
    {
        wp_enqueue_script('cctp-currency', CCTP_URL . 'assets/js/cctp-currency.js', array('jquery'), CCTP_VERSION, true);
        wp_localize_script('cctp-currency', 'data_object', array('domain_url' => get_site_url()));
        $currencies_price_list = cmc_usd_conversions('all');
        $bitcoin_price = cmc_btc_price();
        $c_json = currencies_json();
        $old_currency = "USD";
        $html = "";
        $html .= '<script id="cmc_curr_list" type="application/json">' . esc_html($c_json) . '</script>';
        $html .= '<div class="cmc_price_conversion">
		<select id="cmc_usd_conversion_box" class="cmc_conversions">';
        $currencies_price_list['BTC'] = $bitcoin_price;
        foreach ($currencies_price_list as $name => $price) {
            $csymbol = cmc_old_cur_symbol($name);
            if ($name == $old_currency) {

                $html .= '<option selected="selected" data-currency-symbol="' . esc_attr($csymbol) . '" data-currency-rate="' . esc_attr($price) . '"  value="' . esc_attr($name) . '" >' . esc_html($name) . '</option>';
            } else {
                $html .= '<option data-currency-symbol="' . esc_attr($csymbol) . '" data-currency-rate="' . esc_attr($price) . '"  value="' . esc_attr($name) . '">' . esc_html($name) . '</option>';
            }
        }
        unset($currencies_price_list['BTC']);
        $html .= '</select></div>';

        return $html;

    }

}
CCTP_shortcodes::register();
