import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ClipLoader from "react-spinners/ClipLoader";
import DOMPurify from 'dompurify';
import Select,{createFilter} from 'react-select'

function PortfolioAssetData(props){
  
    const [ Coin, setCoin ] = useState( props.asset );
    const [ TransactionType, setTransactionType ] = useState( 'buy' );
    const [ Quantity, setQuantity ] = useState("");
    const [ CoinPrice, setCoinPrice ] = useState(0);
    const [ CoinFee, setCoinFee ] = useState(0);
    const [ CoinNote, setCoinNote ] = useState("");
    const [ TransactionDate, setDate ] = useState(  new Date().toISOString().slice(0, 16));
    const [Error,setError]=useState({})
    const [Optional,setOptional]=useState(0)
    // const [ TransactionData, setTransactionData ] = useState( '' );
   const getCoin = (coin) =>{
       setCoin(coin)
     props.coins.map(coin_sel=>{
           if(coin_sel.coin_id == coin){  
               let num=(coin_sel.price * props.fiatRate)
               if(num<0.1){
                   num = props.format(coin_sel.price * props.fiatRate)
               }
               else{
                num = (coin_sel.price * props.fiatRate).toFixed(2)
               }
                       setCoinPrice(num)
           }
       })
       if(Object.keys(Error).length !== 0){
        let quantity_error = Error
        delete quantity_error.price
        setError(quantity_error);
       }
   }

   const getTransactiontype = () =>{
       let type = jQuery('.cctp-select-transaction-type').val()
       setTransactionType(type)
   }

   const getOptionalField = () =>{
    let field_show =jQuery('.cctp-optional-field').val()
    setOptional(field_show)
   }


   const QuantityHandler=(e)=>{
       setQuantity(e.target.value)
       let quantity_error = Error
       delete quantity_error.quantity
       setError(quantity_error);
   }
   const PriceHandler=(e)=>{
     
    setCoinPrice(e.target.value)
       let quantity_error = Error
       delete quantity_error.price
       setError(quantity_error);
   }
   const DateHandler=(e)=>{
       
    setDate(e.target.value)
       let quantity_error = Error
       delete quantity_error.date
       setError(quantity_error);
   }


   const handle_validation = ()=>{
       let error = {}
       if(Quantity == ""){
           error['quantity']="Please enter atleast 1 quantity"
       } 
       else if(Quantity == 0){
        error['quantity']="Quantity must be greater than 0"
       }

       if(CoinPrice == ""){
           error['price']="Please enter the price"
       } 
       else if(CoinPrice == 0){
        error['price']="Price must be greater than 0"
       }
       if(TransactionDate == ""){
           error['date']="Enter the date in format dd-mm-yyyy"
       } 

       setError(error)
       if(Object.keys(error).length === 0){
          return true
       }
       else{
            return false
       }
   }

   const CoinSelect = (sel_coin) => {
       let select_coin=""
    if(props.coins != []){
        props.coins.map((coin,index)=>{
            if(sel_coin == coin.coin_id){
                select_coin={ value:coin.coin_id, label:coin.symbol,name:coin.name,logo:coin.logo}
            }
        })
    }
    return select_coin
}
   const initialState = () =>{
    if(props.transaction_operation != "Edit"){
    getCoin(props.asset)
    setTransactionType('buy')
    setQuantity("")
    setCoinFee(0)
    setDate( new Date().toISOString().slice(0, 16))
    setCoinNote("")
    setOptional(0)
    setError({})
    }
   }

   const editTranFun = ()=>{
       let coin =""
       let price = ""
       let quantity = ""
       let trans_type =""
       let trans_date = ""
       let trans_fee = ""
       let trans_note = ""
       props.transactions.map(transaction => {
           if(transaction.transaction_id == props.transaction_id){
               coin=transaction.coin
               price=transaction.price
               quantity=transaction.quantity
               trans_type=transaction.transaction_type
               trans_date =transaction.transaction_date
               trans_fee =transaction.fee
               trans_note =transaction.note
           }
       })
       setCoin(coin)
       if(price<0.1){
        price = props.format(price * props.fiatRate)
        }
        else{
            price = (price * props.fiatRate).toFixed(2)
        }
        setCoinPrice(price)
       setQuantity(quantity)
       setTransactionType(trans_type)
       setDate(trans_date)
       if(trans_fee == "" && trans_note == ""){
           setOptional(0)
       }
       else{
           setOptional(1)
           setCoinFee(trans_fee)
           setCoinNote(trans_note)
       }
       
   }

   const TransactionData =()=>{
    let response= handle_validation()
    let fee = CoinFee
    if(fee !=0){
        fee = CoinFee/props.fiatRate
    }
    let transaction ={
        'coin':Coin,
                'quantity':Quantity,
                'price':CoinPrice/props.fiatRate,
                'fee':fee,
                'note':CoinNote,
                'transaction_type':TransactionType,
                'transaction_date':TransactionDate,
                'transaction_id':props.transaction_num
            }
        if(response == true){
            props.loading_fun()
             props.formFun(transaction)
             initialState()
           
        }
   }
   useEffect(() => {
       if(props.coins[0] !=undefined){
        getCoin(props.asset)
        }
    },[props.asset])

   useEffect(() => {
       if(props.transaction_operation == "Edit"){
            editTranFun()
        }else{
            initialState()
        }
    },[props.transaction_operation])

    const formatOptionLabel = ({ value, label, name,logo }) => (
        <div><img src={logo} className="cctp_logo" /><span>{name} ({label})</span></div>
      );
 

    
    let coins_list = [];
    if(props.coins != []){
                    props.coins.map((coin,index)=>(
                    coins_list.push({ value:coin.coin_id, label:coin.symbol,name:coin.name,logo:coin.logo} )
    ))
    }
    return(
        <React.Fragment>
        <div className="cctp-model-wrapper">
        <div className="cctp-model-content">
            <div className="cctp-model-title">{props.transaction_operation+" Transaction"}<span className="close-btn" onClick={()=>{jQuery('.cctp-model-wrapper').css("display", "none") 
                                                                                                    initialState()}}>X</span></div>
            <div className="cctp-model-form">
                <form >
                <select value={TransactionType} className="cctp-select-transaction-type"  onChange={getTransactiontype}>                       
                    <option value={'buy'} >                                
                            Buy 
                     </option>                 
                    <option value={'sell'} >                                
                        Sell 
                     </option>                 
                        </select>
                    <Select options={coins_list} value={CoinSelect(Coin)} onChange={(e)=>getCoin(e.value)}  className={"cctp-select-coins"} formatOptionLabel={formatOptionLabel} matchProp="any" />
                    {/* <select  className="cctp-select-coins"  onChange={(e)=>getCoin(e.target.value)} value={Coin}>                        
                            {coins_list}                     
                        </select> */}
                    <div className="cctp-data-fields">
                        <label>
                            Quantity<br />
                            <input type="number" placeholder="0.0" min="0" value={Quantity} onChange={(e)=>QuantityHandler(e) } />
                            <span className="cctp-quantity-error" style={{color:"red"}}>{Error.quantity}</span>
                        </label>
                        <label>
                            Price Per Coin<br />
                            <span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.fiatSymbol)}}/>
                            <input type="number" value={CoinPrice} min={0} placeholder="$0.0" onChange={(e)=> PriceHandler( e ) } />
                            <span className="cctp-price-error" style={{color:"red"}}>{Error.price}</span>
                        </label>
                    </div>
                    <div className="cctp-data-fields">
                        <input type="datetime-local" value={TransactionDate} onChange={(e)=>DateHandler(e)}/>
                        <span className="cctp-date-error" style={{color:"red"}}>{Error.date}</span>
                        <select defaultValue={0} className="cctp-optional-field" onChange={getOptionalField}>
                            <optgroup label="Select Optional Field">
                                <option value={0}>
                                   None
                                </option>
                                <option value={1}>
                                Add Fee/ Add Note
                                </option>
                            </optgroup>
                        </select>
                    </div>
                    {Optional == 1  ?
                    <div>
                    <label className="cctp-add-fee">
                        Add Fee<br></br>
                        <span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.fiatSymbol)}}/>
                    <input type="number" min="0" value={CoinFee} placeholder="$"  onChange={(e)=>(setCoinFee(e.target.value))}></input>
                    </label>
                    <label className="cctp-add-note">
                        Add Note<br></br>
                    <textarea value={CoinNote} placeholder="Write your note here...." onChange={(e)=>(setCoinNote(e.target.value))}></textarea>
                    </label>
                    </div>
                    :null
                    }
                    <div className="cctp-total-spent">
                    <span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.fiatSymbol)}}/>
                    <textarea placeholder="Total Spent" readOnly value={props.format(Quantity*CoinPrice + Number(CoinFee))}></textarea>
                    </div>
                    <div className="cctp-clip-loader">
                    <ClipLoader loading={props.loading} size={10}/>
                    </div>
                    <input type="button" value={props.transaction_operation+" Transaction"} onClick={()=>{TransactionData()}} />
                </form>
            </div>
        </div>
    </div>
    </React.Fragment>
    )
}

export default PortfolioAssetData