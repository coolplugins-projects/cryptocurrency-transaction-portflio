import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PortfolioAssetData from './PortfolioAssetData';
import TransactionList from './TransactionList';
import DeleteComponent from './DeleteComponent';
import PortfolioStats from './PortfolioStats';
import DOMPurify from 'dompurify';
import Icon from '../Icon';
// import DataTables from './DataTable';

import DataTable from 'react-data-table-component';
const url = `${appLocalizer.apiUrl}/wp-json/wp/v2/crypto_portfolio`;

const columns = [
    {
        name: 'Name',
        selector:(row)=>row.id,
        cell:(row)=>row.view_name,
        sortable: true,
    },
    {
        name: 'Price',
        selector:(row)=>row.price,
        cell:(row)=><div><span>{row.symbol}</span>{row.price}</div>,
        sortable: true,
    },
    {
        name: '24H',
        selector: row => row.two_four_H,
        cell:(row)=><span className="cctp-percentage">{Math.abs(row.two_four_H)}%</span>,
        sortable: true,
        conditionalCellStyles:[{
            when: row => Number((row.two_four_H).replace(/,(?=\d{3})/g, '')) < 0,
            classNames: ['cctp-price-down']    
        },{
            when: row => Number((row.two_four_H).replace(/,(?=\d{3})/g, '')) > 0,
            classNames: ['cctp-price-up']    
        }
        
        ]
    },
    {
        name: 'Holdings',
        selector: row => row.holdings,
        cell:(row)=><div><span className="cctp-title">{row.symbol}{row.holdings }</span><span className="cctp-symbol">{row.total_quantitys} {row.coin_symbol}</span></div>,
        sortable: true,
    },
    {
        name: 'Avg. Buy Price',
        selector: row => row.avg_buy_price,
        cell:(row)=><div>{row.symbol}{row.avg_buy_price}</div>,
        sortable: true,
    },
    {
        name: 'Profit/Loss',
        selector: row => row.profit_loss,
        cell:(row)=><div><span className="cctp-title">{row.symbol}{row.profit_loss}</span><span className="cctp-percentage-profit-loss">{row.profit_loss_per}%</span></div>,
        sortable: true,
        maxWidth:18+"vh",
        minWidth:0+"vh",
        conditionalCellStyles:[{
            when: row => Number((row.profit_loss).replace(/,(?=\d{3})/g, '')) < 0,
            classNames: ['cctp-price-down'] 
        },{
            when: row => Number((row.profit_loss).replace(/,(?=\d{3})/g, '')) >= 0,
            classNames: ['cctp-price-up']  
        }
        
        ],
    },
    {
        name: 'Actions',
        maxWidth:18+"vh",
        minWidth:0+"vh",
        selector: row => row.actions,
    },
];

 class PortfolioData extends React.Component{
    constructor(props) {
	    super( props);
       
		this.state = {
            transactions: [],
            portfolio_id:"",
            total_portfolio:0,
            transaction_number:0,
            assets:[],
            asset_select:"",
            transactionShow:false,
            totalPrice:0,
            add_transaction_loading:false,
            delete_asset_loading:false,
            delete_transaction_loading:false,
            modal_state:false,
            modal_transaction_delete_state:false,
            transaction_select:0,
            transaction_symbol:"",
            transaction_page_load:false,
            coin_name:"",
            select_transaction_id:0,
            transaction_ops:"Add"
		};
        
	}

    AddTransaction(value){
        if(value == null){
            this.setState({asset_select:this.props.coinData[0].coin_id,transaction_ops:"Add"})
        }
        else{
            this.setState({asset_select:value,transaction_ops:"Add"})
        }
        jQuery('.cctp-model-wrapper').css("display", "flex")
    }
    editTransaction(id){
        this.setState({select_transaction_id:id,transaction_ops:"Edit"})
        jQuery('.cctp-model-wrapper').css("display", "flex")
    }
    fetchTransactions(){
        this.setState({transaction_page_load:true})
        axios.get( url+'/'+this.props.select_portfolio_id,
        {
            headers:{
                'content-type': 'application/json',
                'X-WP-NONCE': appLocalizer.nonce
            }
         } )
        .then( ( res ) => {
            if(res.data.my_field !== undefined){
                let transaction_length = res.data.my_field.length
                let transaction_number = 0
                if(transaction_length != 0){
                    transaction_number = res.data.my_field[transaction_length-1].transaction_id + 1
                }
                this.setState({transactions:res.data.my_field,portfolio_id:this.props.select_portfolio_id,total_portfolio:res.data.total_portfolio,transaction_number:transaction_number,assets:res.data.portfolio_assets , transaction_page_load:false }) 
            }

       } )
    }

    componentDidMount(){
        this.fetchTransactions() 
   }
    
    componentDidUpdate(prevProps, prevState){
        if(this.props.select_portfolio_id !== prevProps.select_portfolio_id ){
            this.fetchTransactions()
            // this.setState({transaction_page_load:true})
        }
    }

    handleSubmit = (value) => {
        let transaction_array = this.state.transactions.slice(0);
        if(this.state.transaction_ops == "Edit"){
            transaction_array.map((transaction ,index)=>{
                if(transaction.transaction_id == this.state.select_transaction_id){
                    transaction_array.splice(index,1)
                }
            })
        }
        transaction_array.push(value)
        let transaction_amount = value.price* value.quantity
        let total_amount = Number(this.state.total_portfolio) + transaction_amount
        let assets_post = this.state.assets.slice(0)
        if(this.state.assets.includes(value.coin) == false){
            assets_post.push(value.coin)
        }
        axios.post(url+'/'+this.props.select_portfolio_id,{
            my_field:transaction_array,
            total_portfolio:total_amount,
            portfolio_assets:assets_post
        },{
            headers:{
                'content-type': 'application/json',
                'X-WP-NONCE': appLocalizer.nonce
            }
        })
        .then( ( res ) => {
             
                jQuery('.cctp-model-wrapper').css("display", "none")
                this.loadingStateChange()
                let transaction_length = res.data.my_field.length
                let transaction_number = 0
                if(transaction_length != 0){
                    transaction_number = res.data.my_field[transaction_length-1].transaction_id + 1
                }
                this.setState({transactions:transaction_array,total_portfolio:total_amount,transaction_number:transaction_number,assets:assets_post})
                this.props.transaction_total()

            } )
   }
   viewTransaction(asset,coin_name,symbol){
    this.setState ({
        coin_name:coin_name,
        asset_select:asset,
        transaction_symbol:symbol
      });
      jQuery('.cctp-model-transactionlist-wrapper').css("display","flex")
   }

   delete_transaction_select(id){
        this.setState ({
            transaction_select:id
          ,modal_transaction_delete_state:true}); 
   }
   delete_asset_select(asset){
    this.setState ({
        asset_select:asset
      ,modal_state:true}); 
   }

   delete_asset(asset){
       this.setState({delete_asset_loading:true})
       let assets=[]
       let transactions=[]
       let filter_transaction=[]
     assets=[...this.state.assets]
     transactions=[...this.state.transactions]
    transactions.map((transaction,index)=>{
        if(asset != transaction.coin){
            filter_transaction.push( transaction );    
        }    
    })
    var index = assets.indexOf(asset);
    if (index >= 0) {
        assets.splice( index, 1 );
    }
    axios.post(url+'/'+this.props.select_portfolio_id,{
        my_field:filter_transaction,
        portfolio_assets:assets,
    },{
        headers:{
            'content-type': 'application/json',
            'X-WP-NONCE': appLocalizer.nonce
        }
    })
    .then( ( res ) => {
    this.setState({assets:assets,transactions:filter_transaction,delete_asset_loading:false,modal_state:false})
    this.props.transaction_total()
    
    })
   }

   deleteTransaction(id){    
    this.setState({delete_transaction_loading:true})
    let transactions =[...this.state.transactions]
    let transaction_index = ""
    let fliter_transaction_amount
    transactions.map((transaction,index)=>{
        if(id == transaction.transaction_id){
            transaction_index = index;
            fliter_transaction_amount = transaction.price* transaction.quantity
                }
            })
    transactions.splice(transaction_index,1)
    transactions.map((transaction,index)=>{
        transaction.transaction_id = index+1
    })
    let total_amount = Number(this.state.total_portfolio) - fliter_transaction_amount
    let flag = 0
            let assets=this.state.assets
            transactions.map(transaction=>{
             if(this.state.asset_select == transaction.coin){
                 flag = 1
             }    
         })
         if(flag == 0){
             var index = assets.indexOf(this.state.asset_select);
                 if (index >= 0) {
                     assets.splice( index, 1 );
                 }
         }
    axios.post(url+'/'+this.props.select_portfolio_id,{
        my_field:transactions,
        portfolio_assets:assets,
        total_portfolio:total_amount
    },{
        headers:{
            'content-type': 'application/json',
            'X-WP-NONCE': appLocalizer.nonce
        }
    })
    .then( ( res ) => {
        
            this.setState({transactions:transactions,total_portfolio:total_amount,transaction_number:res.data.my_field.length +1,assets:assets,delete_transaction_loading:false,modal_transaction_delete_state:false})
            if(flag == 0){
            this.setState({transactionShow:false})
            }
            this.props.transaction_total()
        } )   
   }

   loadingStateChange(){
    this.setState(prevState => ({
        add_transaction_loading: !prevState.add_transaction_loading
      }));
   }

   modal_state_fun(){
       this.setState({modal_state:false})

   }

   modal_transaction_state_fun(){
    this.setState({modal_transaction_delete_state:false})
   }


   render(){
    let array=[]
    let profit_loss_array=[]  
   let table_data=[]
   let total_portfolio_amount = 0
   let total_portfolio_24H_price = 0
   let total_portfolio_24H_per = 0
    if(this.state.assets != []){
        this.state.assets.map(asset=>{
        let total_quantity=0
        let total_price=0
        let total_buy_quantity=0
        let total_buy_price=0
        let total_sell_price=0
        let total_sell_quantity=0
        let avg_buy_price=0
        let avg_sell_price=0
        let profit_loss_buy =0
        let profit_loss_sell =0
        let profit_loss = 0
        let total_quantity_decimal = 0
        let profit_loss_per = 0
        this.props.coinData.map((coin,index)=>{
            let coin_price = 0
            if(this.props.CoinSymbol == "BTC"){
                coin_price = coin.price / this.props.FiatRate
            }
            else{
                coin_price = coin.price * this.props.FiatRate
            }
            let coin_price_filter =Number(this.props.format_fun(coin_price).replace(/,(?=\d{3})/g, ''))

        
            if(coin.coin_id == asset){
                total_portfolio_24H_per += Number(coin.percent_change_24h)
                this.state.transactions.map(transaction=>{
                    let transaction_price = 0
                    if(this.props.CoinSymbol == "BTC"){
                         transaction_price = transaction.price / this.props.FiatRate
                    }
                    else{
                         transaction_price = transaction.price * this.props.FiatRate
                    }
                    if(transaction.coin == coin.coin_id){
                        let numStr = String(transaction.quantity);
                        let transaction_quantity_decimal = 0
                        if (numStr.includes('.')) {
                         transaction_quantity_decimal = numStr.split('.')[1].length;
                         };
                         if(total_quantity_decimal < transaction_quantity_decimal){
                            total_quantity_decimal = transaction_quantity_decimal
                        }
                        if(transaction.transaction_type == 'buy'){
                        total_quantity = (total_quantity + Number(transaction.quantity))
                        total_buy_quantity = total_buy_quantity + Number(transaction.quantity)
                        total_buy_price = total_buy_price + (Number(transaction_price) * Number(transaction.quantity))
                    }
                    else{
                        total_quantity = (total_quantity - Number(transaction.quantity))
                       
                            total_sell_price = total_sell_price + (Number(transaction_price) * Number(transaction.quantity))
                            total_sell_quantity = total_sell_quantity + Number(transaction.quantity)
                        }
                        total_price = total_price + (Number(transaction_price) * Number(transaction.quantity))
                    }
                })
                total_portfolio_amount = total_portfolio_amount + (Number(coin.price) * Number(total_quantity))
                if(total_buy_quantity != 0){
                    avg_buy_price=total_buy_price/total_buy_quantity
                }
                if(total_sell_quantity != 0){
                    avg_sell_price=total_sell_price/total_sell_quantity
                }

                profit_loss_buy =(coin_price_filter - Number(this.props.format_fun(avg_buy_price).replace(/,(?=\d{3})/g, ''))) * total_buy_quantity
                profit_loss_sell =( Number(this.props.format_fun(avg_sell_price-coin_price_filter).replace(/,(?=\d{3})/g, ''))) * total_sell_quantity
                
                profit_loss = Number(this.props.format_fun(profit_loss_buy).replace(/,(?=\d{3})/g, ''))+ Number(this.props.format_fun(profit_loss_sell).replace(/,(?=\d{3})/g, ''))
                if(profit_loss != 0 && total_buy_price != 0){
                    profit_loss_per = (profit_loss)/(total_buy_price)  * 100
                }
                array.push({"profit_loss":profit_loss ,"profit_loss_percentage":profit_loss_per,"logo":coin.logo,"name":coin.name,"portfolio_id":this.props.select_portfolio_id,avg_price:total_buy_price})
                profit_loss_array.push(profit_loss )
            
                let data ={
                    id: coin.coin_id,
                        logo:coin.logo,
                        symbol:this.props.CoinSymbol,
                        name:coin.name,                            
                        price: this.props.format_fun(coin_price),
                        two_four_H: this.props.format_fun(coin.percent_change_24h),
                        holdings: this.props.format_fun(total_quantity * coin_price) ,
                        total_quantitys:total_quantity.toFixed(total_quantity_decimal),
                        coin_symbol:coin.symbol,
                        avg_buy_price:this.props.format_fun(avg_buy_price),
                        profit_loss:this.props.format_fun(profit_loss),
                        profit_loss_per:this.props.format_fun(profit_loss_per),
                        view_name:<div className="cctp-name" onClick={()=>this.viewTransaction(asset,coin.name,coin.symbol)}><div className="cctp-coin-detail"> <img src={coin.logo} className="cctp_logo" /><span className="cctp-name_symbol">({coin.symbol})</span></div> <span className="cctp_coin_symbol">{coin.name}</span></div>,
                        actions: <div><span name="Add Transaction" className="cctp-add-asset" onClick={()=>this.AddTransaction(asset)}>{Icon().AddTransaction}</span><span name="View Transactions" className="cctp-view-asset" onClick={()=>this.viewTransaction(asset,coin.name,coin.symbol)}>{Icon().ViewTransaction} </span><span name="Remove Asset"className="cctp-delete-asset"onClick={()=>this.delete_asset_select(asset)}>{Icon().DeleteIcon}</span>
                        </div>
                    }
                table_data.push(data) 
            
            }
        })
        if(total_portfolio_24H_per != 0){
            total_portfolio_24H_per = total_portfolio_24H_per/this.state.assets.length
            total_portfolio_24H_price = (total_portfolio_amount * total_portfolio_24H_per) /100
        }
    })
 
    }

    return(
        <React.Fragment>
        {this.state.transaction_page_load == false ?   
         <div className="cctp-asset-wrapper">
    <div className="cctp-portfolio-lists">
            <div className="cctp-row">
            <div className="cctp-crntprice">
                    Current Balance
                </div>
            </div>
            <div className="cctp-row">
                <div className="cctp-total-price">
                    {/* <div className="cctp-total-portfolio"> */}
                    <span className="price"><span dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(this.props.CoinSymbol)}}/> {this.props.format_fun(total_portfolio_amount * this.props.FiatRate)}</span>
                    {/* <span className="changes">{this.props.format_fun(total_portfolio_24H_per)}%</span> */}
                    {/* </div> */}
                    {/* <div  className="cctp-total-portfolio-24h" >
                        <span className="cctp-total-24h">{this.props.format_fun(total_portfolio_24H_price * this.props.FiatRate)} </span><span className="cctp-24h-logo">24h</span>
                    </div> */}
                </div>
                <div className="cctp-btn">
                    <button className="addnew-btn" onClick={()=>this.AddTransaction(null) }>+ Add Transaction </button>
                </div>
            </div>
            {this.state.transactions.length !=0 ?
            <div>
            <PortfolioStats   arrays={array} array_profit_loss={profit_loss_array} fiatSymbol={this.props.CoinSymbol} format={(value)=>this.props.format_fun(value)} />
            <h3 className="cctp-asset-title">Your Assets</h3>  
               
        {this.state.assets .length <= 10 ?
        <div className = "cctp-asset-table">
        <DataTable
            columns={columns}
            data={table_data}
            highlightOnHover='yes'
            defaultSortFieldId={1} 

        />
        </div>
        :
        <div className = "cctp-asset-table pagination">
        <DataTable
            columns={columns}
            data={table_data}
            highlightOnHover='yes'
            pagination
            defaultSortFieldId={1} 

        />
        </div>
            }
        <TransactionList assetName ={this.state.coin_name} asset={this.state.asset_select} transactions={this.state.transactions} delete_trans={(value)=>this.delete_transaction_select(value)} fiatSymbol={this.props.CoinSymbol}  fiatRate={this.props.FiatRate} coin_symbol={this.state.transaction_symbol} format={(value)=>this.props.format_fun(value)} add_trans={(value)=>this.AddTransaction(value)} edit_trans={(value)=>this.editTransaction(value)}  />
        </div>:
        <div className="cctp-empty-transaction">  
        <span>This Portfolio is empty</span><br/>
        <span>Add any coins to get started</span>
            <div className="cctp-btn">
                <button className="addnew-btn" onClick={()=>this.AddTransaction(null) }>+ Add Transaction </button>
            </div>
        </div>
            }
    </div>
        <PortfolioAssetData formFun={(value)=>this.handleSubmit(value)} transactions={this.state.transactions} transaction_num={this.state.transaction_number} coins={this.props.coinData} loading={this.state.add_transaction_loading} loading_fun={()=>this.loadingStateChange()} fiatSymbol={this.props.CoinSymbol} fiatRate={this.props.FiatRate} format={(value)=>this.props.format_fun(value)} asset={this.state.asset_select} transaction_id={this.state.select_transaction_id} transaction_operation={this.state.transaction_ops}/>
        
        {/* Delete Asset */}
        {this.state.modal_state == true &&
            <DeleteComponent loader={this.state.delete_asset_loading} id={this.state.asset_select} deleteFun={(id)=>this.delete_asset(id)} component="Asset" modalClose={(value)=>this.modal_state_fun(value)} />
        }
        {this.state.modal_transaction_delete_state == true &&
            <DeleteComponent loader={this.state.delete_transaction_loading} id={this.state.transaction_select} deleteFun={(id)=>this.deleteTransaction(id)} component="Transaction" modalClose={(value)=>this.modal_transaction_state_fun(value)} />      
       }    
       </div>
     :  
    <div className="cctp-transaction-loader">
        <div className="ph-item">     
            <div className="ph-col-12">
                <div className="ph-row">
                    <div className="ph-col-6 big"></div>
                    <div className="ph-col-4  big"></div>
                    <div className="ph-col-2 big"></div>
                    <div className="ph-col-4"></div>
                    <div className="ph-col-8 "></div>
                    <div className="ph-col-6"></div>
                    <div className="ph-col-6 "></div>
                    <div className="ph-col-12"></div>
                </div>
            </div>
        </div>
    </div>   
    
 }       
    </React.Fragment>

        )
    }
}
export default PortfolioData