import React, { useState, useEffect, cloneElement } from 'react';
import axios from 'axios';
import DOMPurify from 'dompurify';
const url = `${appLocalizer.apiUrl}/wp-json/wp/v2/crypto_portfolio`;
function PortfolioStats(props){
    const [ BestPerformanceName, setBestPerformanceName] = useState("");
    const [ WorstPerformanceName, setWorstPerformanceName] = useState("");
    const [ BestCoinLogo, setBestCoinLogo ] = useState(0);
    const [ WorstCoinLogo, setWorstCoinLogo ] = useState(0);
    const [ BestCoinPrice, setBestCoinPrice ] = useState(0);
    const [ WorstCoinPrice, setWorstCoinPrice ] = useState(0);
    const [ BestCoinPercentage, setBestCoinPercentage ] = useState(0);
    const [ WorstCoinPercentage, setWorstCoinPercentage ] = useState(0);
    const [ AllTimeProfit, setAllTimeProfit ] = useState(0);
    const [ AllTimePercentage, setAllTimePercentage ] = useState(0);

    const  postPerformanceData=(value,id)=>{
        axios.post(url+'/'+id,{
            "portfolio_best_worst":value,
            // author:appLocalizer.author_id,
        },{
            headers:{
               'content-type': 'application/json',
                'X-WP-NONCE': appLocalizer.nonce
            }
        })
       
        }

    useEffect(() => {
        let best_performance_name=""
        let worst_performance_name=""
        let all_time_profit=0
        let best_profit_loss=0
        let best_profit_loss_per=0
        let best_profit_logo=0
        let worst_profit_loss=0
        let worst_profit_loss_per=0
        let worst_profit_logo=0
        let portfolio_id=0
        let performance_array={}
        let all_time_profit_percentage=0
        let total_avg_price=0
        if(props.arrays.length != 0 ){
            best_profit_loss=Math.max(...props.array_profit_loss)
            worst_profit_loss=Math.max(...props.array_profit_loss)
            props.arrays.map(array=>{
                total_avg_price += Number(array.avg_price)
                all_time_profit += Number(array.profit_loss)
                if(props.arrays.length == 1){
                    best_performance_name=array.name
                    best_profit_loss = array.profit_loss
                    best_profit_loss_per = array.profit_loss_percentage
                    best_profit_logo = array.logo
                    worst_performance_name=array.name
                    worst_profit_loss = array.profit_loss
                    worst_profit_loss_per = array.profit_loss_percentage
                    worst_profit_logo = array.logo
                }
                else{
                if(best_profit_loss <= Number(array.profit_loss)){
                    best_performance_name=array.name
                    best_profit_loss = array.profit_loss
                    best_profit_loss_per = array.profit_loss_percentage
                    best_profit_logo = array.logo
                }

                if(worst_profit_loss >= Number(array.profit_loss)){
                    worst_performance_name=array.name
                    worst_profit_loss = array.profit_loss
                    worst_profit_loss_per = array.profit_loss_percentage
                    worst_profit_logo = array.logo
                }
            }
                portfolio_id=array.portfolio_id
            })
            if(total_avg_price != 0){
                all_time_profit_percentage=all_time_profit/total_avg_price * 100
            }
        }
        setBestPerformanceName(best_performance_name)
        setBestCoinLogo(best_profit_logo)
        setBestCoinPrice(best_profit_loss)
        setBestCoinPercentage(best_profit_loss_per)
        setWorstPerformanceName(worst_performance_name)
        setWorstCoinLogo(worst_profit_logo)
        setWorstCoinPrice(worst_profit_loss)
        setWorstCoinPercentage(worst_profit_loss_per)
        setAllTimeProfit(all_time_profit)
        setAllTimePercentage(all_time_profit_percentage)
        performance_array={
            "bestPerformanceName":best_performance_name,
            "bestPerformancePrice":best_profit_loss,
            "bestPerformancePercentage":best_profit_loss_per,
            "bestPerformanceLogo":best_profit_logo,
            "worstPerformanceName":worst_performance_name,
            "worstPerformancePrice":worst_profit_loss,
            "worstPerformancePercentage":worst_profit_loss_per,
            "worstPerformanceLogo":worst_profit_logo,
            "allTimePrice":all_time_profit,
            "allTimePercentage":all_time_profit_percentage
        }
        if(portfolio_id !== 0){
        postPerformanceData(performance_array,portfolio_id)
        }
     },[props.arrays])
     
    
    return(
        <div className="cctp-row">
        <div className="cctp-performance">
            <span className="cctp-title">All Time Profit</span>
            {AllTimeProfit < 0 ?
            <span className="changes-down">{props.format(Math.abs(AllTimePercentage))}% (<span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.fiatSymbol)}}/>{props.format(AllTimeProfit)})</span>
            :
            <span className="changes-up">{props.format(Math.abs(AllTimePercentage))}% (<span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.fiatSymbol)}}/>{props.format(AllTimeProfit)})</span>
            }
        </div>
        <div className="cctp-performance">
            <img className="cctp-best-coin-logo" src={BestCoinLogo} alt={BestPerformanceName}  />
            <div className="best-performance">
            <span className="cctp-title">Best Performer</span>
            {BestCoinPrice < 0 ?
            <span className="changes-down">{props.format(Math.abs(BestCoinPercentage))}% (<span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.fiatSymbol)}}/>{props.format(BestCoinPrice)})</span>
            :
            <span className="changes-up">{props.format(Math.abs(BestCoinPercentage))}% (<span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.fiatSymbol)}}/>{props.format(BestCoinPrice)})</span>
            }
            </div>
        </div>
        <div className="cctp-performance">
            <img className="cctp-worst-coin-logo" src={WorstCoinLogo} alt={WorstPerformanceName} />
            <div className="worst-performance">
            <span className="cctp-title">Worst Performer</span>
            {WorstCoinPrice < 0 ?
            <span className="changes-down">{props.format(Math.abs(WorstCoinPercentage))}% (<span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.fiatSymbol)}}/>{props.format(WorstCoinPrice)})</span>
            :
            <span className="changes-up">{props.format(Math.abs(WorstCoinPercentage))}% (<span dangerouslySetInnerHTML={{__html:DOMPurify.sanitize(props.fiatSymbol)}}/>{props.format(WorstCoinPrice)})</span>
            }
            </div>
        </div>
    </div>
    )

}
export default PortfolioStats