import React, { useState, useEffect } from 'react';
import ClipLoader from "react-spinners/ClipLoader";


function AddComponent(props){
    const [ Name, setName ] = useState( props.name );
    const [ Error, setError ] = useState({})
   

    const handleValidation=()=>{
        let error={}
        if(Name == ""){
            error['name']="Please enter the name"
        } 
        setError(error)
        if(Object.keys(error).length === 0){
            return true
        }
        else{
            return false
        } 
    }
    const inputHandler=(e)=>{
        setName( e.target.value )
        setError({})
    }
    const submit=()=>{
        let response=handleValidation()
        if(response === true){
           return props.create_portfolio(Name)
        }
    }
    return(
    <div className="cctp-model-edit-wrapper">
        <div className="cctp-model-content">
            <div className="cctp-model-title">{props.action} Portfolio Name<span className="close-btn" onClick={()=>props.modalClose(false)}>X</span></div>
            <div className="cctp-model-form">
                <form onSubmit={e => e.preventDefault()}>
                    <label>Portfolio name</label>
                   <input id="portfolio" name="portfolio" value={Name} onChange={(e)=>{inputHandler(e)} } className="regular-text"  />
                   <span className="cctp_submit_error" style={{color:"red"}}>{Error.name}</span>
                   <div className="cctp-clip-loader">
                   <ClipLoader loading={props.loader}  size={10} />
                   </div>
                   <button className="cctp-add-modal" onClick={()=>submit()}>Submit</button>
                </form>
            </div>
        </div>
    </div>
    )
}

export default AddComponent