    
import React, { useState, useEffect } from 'react';
import ClipLoader from "react-spinners/ClipLoader";


function DeleteComponent(props){
//     let override = 
//     position: absolute;
//     bottom: 10px;
//     left: 30%;
//     width: 30px;
//     height: 30px;
//     border-color: #fff;
//     border-bottom-color: #3861fb;
//   `;
    return (
    <div className="cctp-model-delete-wrapper">
    <div className="cctp-model-content">
        <div className="cctp-model-title">Remove {props.component}<span className="close-btn" onClick={()=>props.modalClose(false)}>X</span></div>
        <div className="cctp-model-form">
            <p>Do you want to delete your {props.component}? Performing this action you understand that you would not be able to recover your {props.component}.</p>
            <ClipLoader loading={props.loader}  size={10}  />
               <button className="cctp-delete-button-comp" onClick={()=>props.deleteFun(props.id)}>Remove</button>
               <button className="cctp-cancel-delete-button-comp" onClick={()=>props.modalClose(false)}>Cancel</button>
        </div>
    </div>
</div>
    )
}

export default DeleteComponent