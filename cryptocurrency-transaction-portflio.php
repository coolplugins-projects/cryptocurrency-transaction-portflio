<?php

/**
 * Plugin Name: Cryptocurrency Portfolio Tracker
 * Description: Keep track of your profits, losses and portfolio valuation with our easy to use this plugin.
 * Author: Cool Plugins 
 * Author URI: https://coolplugins.net/
 * Version: 1.0
 * License: GPL2
 * Text Domain: cctp
 * Domain Path: languages
 *
 * @package CCTP_final
 */

namespace CCTP_final;

if (!defined('ABSPATH')) {
    exit;
}

if (defined('CCTP_VERSION')) {
    return;
}

define('CCTP_VERSION', '1.0.0');
define('CCTP_FILE', __FILE__);
define('CCTP_PATH', plugin_dir_path(CCTP_FILE));
define('CCTP_URL', plugin_dir_url(CCTP_FILE));

register_activation_hook(CCTP_FILE, array('CCTP_final\CCTP_final', 'activate'));
register_deactivation_hook(CCTP_FILE, array('CCTP_final\CCTP_final', 'deactivate'));

/**
 * Class CCTP_final
 */
final class CCTP_final
{

    /**
     * Plugin instance.
     *
     * @var CCTP_final
     * @access private
     */
    private static $instance = null;

    /**
     * Get plugin instance.
     *
     * @return CCTP_final
     * @static
     */
    public static function get_instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Constructor.
     *
     * @access private
     */
    private function __construct()
    {
      
    }
   
    public function registers() {
        $this->includes();       
        add_action( 'wp_enqueue_scripts',array( $this,'portfolio_register_scripts') );
         if (is_admin()) {
                add_action('admin_init', array($this, 'cctp_is_cmc_active'));
            }
    }

       public function cctp_is_cmc_active()
        {
            if (!is_plugin_active('coin-market-cap/coin-market-cap.php')) {
                deactivate_plugins('cryptocurrency-transaction-portflio/cryptocurrency-transaction-portflio.php');

                add_action('admin_notices', function () {?>
			<style>div#message.updated {
				display: none;
			}</style>
				<div class="notice notice-error is-dismissible">
					<p><?php
                        _e('<strong>Deactivated</strong>:-<strong>"Cryptocurrency Transaction Portflio"</strong> Coins MarketCap needs to be active to use <strong>Cryptocurrency Transaction Portflio</strong> plugin', 'cctp');
                    ?>
					</p>
				</div>

			<?php });
            }
        }

    /**
     * Load plugin function files here.
     */
    public function includes()
    {       

        require_once CCTP_PATH . '/admin/register-post-type.php'; 
        require_once CCTP_PATH . '/admin/cctp-rest-settings.php';
        require_once CCTP_PATH . '/includes/cctp-shortcode.php';


       
    }

    public function portfolio_register_scripts(){
        wp_register_style('cctp-portfolio-list-css', CCTP_URL . 'assets/css/portfolio-list-style.css', null);
        wp_register_style('cctp-portfolio-modal-css', CCTP_URL . 'assets/css/model-style.css', null);   
        wp_register_style('cctp-portfolio-loader', CCTP_URL . 'assets/css/loader.css', null);
     

    }
   
   
    /**
     * Code you want to run when all other plugins loaded.
     */
    public function init()
    {
        load_plugin_textdomain('cctp', false, CCTP_FILE . 'languages');
      
    }

    /**
     * Run when activate plugin.
     */
    public static function activate()
    {
        
    }

    /**
     * Run when deactivate plugin.
     */
    public static function deactivate()
    {

    }
}


$cctp=CCTP_final::get_instance();
$cctp->registers();
